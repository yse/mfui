#pragma once
#include "mfcore.hpp"

typedef enum{
    Normal=0,Select,On
}ListState;

class BaseListView;

class ListItem{
    public:
    virtual void UpdatePosition(long long n);
    //rect为限定渲染区域
    virtual void Draw(Renderer* render,ListState state,SDL_Rect& rect)=0;
    virtual void _click(BaseListView* list,int x,int y,unsigned char key);
    virtual void _double_click(BaseListView* list,int x,int y,unsigned char key);
    virtual void _on(BaseListView* list,int x,int y);
    virtual void OnAdd(vector<ListItem*>* l);//在此添加到列表中
    virtual ~ListItem();
};

class BaseListView:public Control{
    protected:
    SDL_Texture* _t;//每项缓存
    vector<ListItem*> items;
    long long cur_item;
    int max_item;//最多显示的项数
    public:
    SDL_Color background,select_color,on_color;

    size_t count();//项数
    vector<ListItem*>* GetItemList();

    virtual ListItem* AddItem(ListItem* item);
    //将指针添加到list中
    virtual void AddItem(vector<ListItem*>* src);

    BaseListView(Window* window);

    //越界抛出out_of_range
    ListItem* GetItem(size_t index);
    //越界抛出out_of_range 返回被移出的Item 需要手动析构
    virtual ListItem* RemoveItem(size_t index);

    void Clear(bool freed=true);

    virtual void ItemChanged()=0;//由开发者手动触发

    ~BaseListView()override;
};