#pragma once
#include "image.hpp"

class BetterImage:public Image{
    double angle;
    SDL_RendererFlip flip;
    public:
    void _draw();
    BetterImage(Window* w,SDL_Point p,char* path,double angle,
    SDL_RendererFlip flip);
    ~BetterImage();
};