#include "listview.hpp"

void TableItem::Draw(Renderer* render,ListState state,SDL_Rect& rect){}
TableItem::~TableItem(){
    for(auto& n:children)delete n;
}


void TableView::TableListenMouse(void* list){
    auto &r=((TableView*)list)->_area;
    int x,y;
    SDL_GetMouseState(&x,&y);
    if(x<r.x||x>r.x+r.w||y<r.y||y>r.y+r.h){
        ((TableView*)list)->on=-1;
        ((TableView*)list)->onx=-1;
        ((TableView*)list)->_in=false;
        ((TableView*)list)->_belong->RemoveTask(((TableView*)list)->_task);
        ((TableView*)list)->scroll_color=((TableView*)list)->normal_scroll_color;
        ((TableView*)list)->_task=NULL;
        SDL_SetCursor(arrow_cursor);
        return;
    }
    auto &g=((TableView*)list)->scroll;
    if(x<g.x){
        //项
        ((TableView*)list)->scroll_color=((TableView*)list)->normal_scroll_color;
        ((TableView*)list)->on=((TableView*)list)->cur_item+
        (y-r.y-((TableView*)list)->space)/(((TableView*)list)->item_height+((TableView*)list)->space);
        ((TableView*)list)->onx=(x-r.x)/(((TableView*)list)->item_w+((TableView*)list)->spacex);
        if(((TableView*)list)->on>=((TableView*)list)->items.size()){
            ((TableView*)list)->on=-1;
            ((TableView*)list)->onx=-1;
            SDL_SetCursor(arrow_cursor);
            return;
        }
        auto& a=((TableItem*)((TableView*)list)->GetItem(((TableView*)list)->on))->children;
        if(((TableView*)list)->onx>=a.size()){
            ((TableView*)list)->onx=-1;
            SDL_SetCursor(arrow_cursor);
            return;
        }
        a[((TableView*)list)->onx]->_on((TableView*)list,x,y);
        SDL_SetCursor(button_cursor);
        return;
    }
    ((TableView*)list)->on=-1;
    ((TableView*)list)->onx=-1;
    if(g.x<=x&&x<=g.x+r.w&&g.y<=y&&y<=g.y+g.h){
        ((TableView*)list)->scroll_color=((TableView*)list)->on_scroll_color;
        return;
    }
    ((TableView*)list)->scroll_color=((TableView*)list)->normal_scroll_color;
}
TableView::TableView(Window* window,SDL_Rect& rect,int spacex,int spacey,int item_w,int item_h,
SDL_Color background,SDL_Color select_color,SDL_Color on_color,
SDL_Color normal_scroll_color,SDL_Color on_scroll_color):
ListView(window,rect,spacey,item_h,background,select_color,on_color,normal_scroll_color,on_scroll_color),
item_w(item_w),spacex(spacex){
    selectx=-1;
    onx=-1;
}
void TableView::_release(int x,int y,int clicks,unsigned char key){
    if(!_enabled){
        return;
    }
    auto t=y-_area.y;
    if(t<space||t>_area.h-space){
        return;
    }
    if(x-_area.x>_area.w-10){
        //滚动条
        return;
    }
    if(on==-1){
        return;
    }
    select=on;
    selectx=onx;
    if(clicks==1){
        ((TableItem*)items.operator[](select))->children.operator[](selectx)->_click(this,x,y,key);
        return;
    }
    ((TableItem*)items.operator[](select))->children.operator[](selectx)->_double_click(this,x,y,key);
}
void TableView::_mouse_move(int x,int y){
    if(!_task&&_enabled){
        _in=true;
        _task=_belong->AddTask(TableListenMouse,this,1,false,true);
    }
}
void TableView::_draw(){
    auto r=_belong->renderer;
    r->SetColor(background);
    r->FillRect(_area);
    auto h=item_height;
    SDL_Rect rect{_area.x,_area.y,_area.w,h},_r;
    _r.w=item_w,_r.h=h,_r.y=0;
    auto t=_t;
    ListState state;
    for(auto item=items.begin()+cur_item,end=items.end(),stop=item+max_item,
    selected=items.begin()+select,on_item=items.begin()+on;item!=end;item++){
        if(item==stop){
            r->SetTarget();
            break;
        }
        r->SetTarget(t);
        auto& a=((TableItem*)*item)->children;
        _r.x=0;
        for(size_t i=0,max=a.size();i!=max;i++){
            if(_r.x>_r.w)break;
            if(item==selected&&i==selectx){
                state=ListState::Select;
                r->SetColor(select_color);
            }
            else if(item==on_item&&i==onx){
                state=ListState::On;
                r->SetColor(on_color);
            }
            else{
                state=ListState::Normal;
                r->SetColor(background);
            }
            r->FillRect(_r);
            a[i]->Draw(r,state,_r);
            _r.x+=item_w+spacex;
        }
        r->SetTarget(NULL);
        r->DrawTexture(NULL,&rect,t);
        rect.y+=h+space;
    }
    if(!_in){
        return;
    }
    //绘制滚动条
    r->SetColor(scroll_color);
    r->FillRect(scroll);
}
TableView::~TableView(){}