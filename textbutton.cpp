#include "textbutton.hpp"

Font* TextButton::font(){return _font;}
const char* TextButton::text(){return _text;}
SDL_Color TextButton::text_color(){return _text_color;}
//所有bool属性默认为true
TextButton::TextButton(Window* window,SDL_Point p,const char* text,
SDL_Color text_color,Font* font):Control(window),_text_color(text_color),
_text(text),_font(font){
    _area.x=p.x,_area.y=p.y;
    _t=window->renderer->RenderText(font,text,text_color);
    SDL_QueryTexture(_t,0,0,&_area.w,&_area.h);
}
TextButton::TextButton(Window* window):Control(window){
    _font=default_font;
    _text_color=Color::Black;
    _t=window->renderer->RenderText(default_font,"button",Color::Black);
    SDL_QueryTexture(_t,0,0,&_area.w,&_area.h);
}

void TextButton::SetText(const char* new_text,Font* font,SDL_Color color){
    SDL_DestroyTexture(_t);
    _t=_belong->renderer->RenderText(font,new_text,color);
    SDL_QueryTexture(_t,0,0,&_area.w,&_area.h);
    _font=font,_text=new_text,_text_color=color;
}
void TextButton::_release(int x,int y,int clicks,unsigned char key){
    if(clicks==1){
        OnClick(x,y,key);
        return;
    }
    OnDoubleClick(x,y,key);
}
void TextButton::_draw(){
    _belong->renderer->DrawTexture(0,&_area,_t);
}
TextButton::~TextButton(){SDL_DestroyTexture(_t);}