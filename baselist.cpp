#include "baselist.hpp"

void ListItem::_click(BaseListView* list,int x,int y,unsigned char key){}
void ListItem::_double_click(BaseListView* list,int x,int y,unsigned char key){}
void ListItem::_on(BaseListView* list,int x,int y){}
void ListItem::OnAdd(vector<ListItem*>* l){l->push_back(this);}
ListItem::~ListItem(){}

size_t BaseListView::count(){return items.size();}//项数
vector<ListItem*>* BaseListView::GetItemList(){return &items;}

ListItem* BaseListView::AddItem(ListItem* item){
    item->OnAdd(&items);
    return item;
}
//将指针添加到list中
void BaseListView::AddItem(vector<ListItem*>* src){
    auto& i=items;
    i.reserve(items.size()+src->size());
    for(auto item=src->begin(),end=src->end();item!=end;item++){
        (*item)->OnAdd(&i);
    }
}

BaseListView::BaseListView(Window* window):Control(window){}
void ListItem::UpdatePosition(long long n){}
//越界抛出out_of_range
ListItem* BaseListView::GetItem(size_t index){
    if(index<items.size()){
        return items.operator[](index);
    }
    throw out_of_range(to_string(index));
}
//越界抛出out_of_range 返回被移出的Item 需要手动析构
ListItem* BaseListView::RemoveItem(size_t index){
    if(index<items.size()){
        cur_item=0;
        auto i=items.operator[](index);
        items.erase(items.begin()+index);
        return i;
    }
    throw out_of_range(to_string(index));
}

void BaseListView::Clear(bool freed){
    if(freed){
        for(auto item=items.begin(),end=items.end();item!=end;item++){
            delete *item;
        }
    }
    items.clear();
}

BaseListView::~BaseListView(){
    SDL_DestroyTexture(_t);
    for(auto& n:items){
        delete n;
    }
}