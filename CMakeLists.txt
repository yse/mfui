project(mfui)

add_library(mfui tableview.cpp MFUI.cpp button.cpp canvas.cpp checkbox.cpp droplist.cpp
image.cpp listview.cpp mfcore.cpp mfmenu.cpp label.cpp baselist.cpp progressbar.cpp
textutil.cpp betterimage.cpp textbutton.cpp inputbox.cpp)

target_link_libraries(mfui -lSDL2Main -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer)
