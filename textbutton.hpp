#pragma once
#include "mfcore.hpp"

class TextButton:public Control{
    protected:
    const char* _text;
    SDL_Texture* _t;
    Font* _font;
    SDL_Color _text_color;
    virtual void OnClick(int x,int y,unsigned char key)=0;
    virtual void OnDoubleClick(int x,int y,unsigned char key)=0;
    public:
    Font* font();
    const char* text();
    SDL_Color text_color();
    //所有bool属性默认为true
    TextButton(Window* window,SDL_Point p,const char* text="button",
    SDL_Color text_color=Color::Black,Font* font=default_font);
    TextButton(Window* window);

    void SetText(const char* new_text,Font* font=default_font,SDL_Color color=Color::Black);
    void _release(int x,int y,int clicks,unsigned char key);
    void _draw();
    ~TextButton()override;
};