#include "checkbox.hpp"

CheckBox::CheckBox(Window* window,SDL_Rect& rect,bool init,SDL_Color back,
SDL_Color line,SDL_Color disable):Control(window){
    background=back;
    line_color=line;
    value=init;
    disable_color=disable;
    _area=rect;
}
CheckBox::CheckBox(Window* window):Control(window){
    background=Color::White;
    line_color=Color::Black;
    value=false;
    disable_color=Color::Gray;
}
void CheckBox::_press(int x,int y,int clicks,unsigned char key){}
void CheckBox::_release(int x,int y,int clicks,unsigned char key){
    value=!value;
    OnChanged();
}
void CheckBox::_mouse_move(int x,int y){}
void CheckBox::_draw(){
    auto r=_belong->renderer;
    if(!_enabled){
        r->SetColor(disable_color);
        r->FillRect(_area);
        return;
    }
    r->SetColor(background);
    r->FillRect(_area);
    r->SetColor(line_color);
    r->DrawRect(_area);
    if(value){
        SDL_Point p1{_area.x,_area.y},p2{p1.x+_area.w,p1.y};
        SDL_Point p3{p1.x,p1.y+_area.h},p4{p2.x,p3.y};
        r->DrawLine(p1,p4);
        r->DrawLine(p2,p3);
    }
}
CheckBox::~CheckBox(){}