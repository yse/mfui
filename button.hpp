#pragma once
#include "mfcore.hpp"
class Button:public Control{
    protected:
    enum button_state{
        button_up=0,button_down,button_on
    };
    static void CheckState(void* button);
    static void Pop(void* button);
    const char* _text;
    SDL_Texture* _t;
    Font* _font;
    SDL_Color _text_color;
    virtual void OnClick(int x,int y,unsigned char key)=0;
    virtual void OnDoubleClick(int x,int y,unsigned char key)=0;
    public:
    Task* _task;//勿动
    int _state;//勿动
    SDL_Color release_color,press_color,on_color;
    Font* font();
    const char* text();
    SDL_Color text_color();
    //所有bool属性默认为true
    Button(Window* window,SDL_Rect &rect,const char* text="button",
    SDL_Color release=Color::Green,SDL_Color press=Color::Red,SDL_Color on=Color::Blue,
    SDL_Color text_color=Color::Black,Font* font=default_font);
    Button(Window* window);

    void SetText(const char* new_text,Font* font=default_font,SDL_Color color=Color::Black);

    void _press(int x,int y,int clicks,unsigned char key);

    void _release(int x,int y,int clicks,unsigned char key);
    void _mouse_move(int x,int y);
    void _draw();
    ~Button()override;
};