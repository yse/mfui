#pragma once
#include "mfcore.hpp"

class ProgressBar:public Control{
    double value;
    protected:
    //0~1 横向
    virtual void OnChanged(double value)=0;
    public:
    int bound;//边框厚度
    SDL_Color back,front;
    //有效范围0~1
    bool SetProgress(double v);
    double GetProgress();

    void _mouse_move(int x,int y);
    void _draw();
    void _press(int x,int y,int clicks,unsigned char key);

    ProgressBar(Window* w,SDL_Rect& r,double init=1.,SDL_Color back=Color::Black,
    SDL_Color front=Color::ManganeseBlue,int bound=3);
    ProgressBar(Window* w);
    ~ProgressBar()override;
};