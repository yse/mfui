#include "image.hpp"

Image::Image(Window* window,SDL_Rect& rect,const char* path):Control(window){
    _area=rect;
    follow=false;
    _t=window->renderer->LoadTexture(path);
    if(!_t){
        printf("Err:File %s not exist\n",path);
        throw FileNotFoundException();
    }
}
Image::Image(Window* window,SDL_Point pos,const char* path):Control(window){
    _area.x=pos.x,_area.y=pos.y;
    follow=true;
    _t=window->renderer->LoadTexture(path);
    if(!_t){
        printf("Err:File %s not exist\n",path);
        throw FileNotFoundException();
    }
    SDL_QueryTexture(_t,NULL,NULL,&_area.w,&_area.h);
}
Image::Image(Window* window,unsigned int w,unsigned int h):Control(window){
    follow=true;
    _t=window->renderer->CreateTexture(w,h);
    SDL_SetTextureBlendMode(_t,SDL_BLENDMODE_BLEND);
    auto r=window->renderer;
    r->SetColor({255,255,255,0});
    r->SetTarget(_t);
    r->Clear();
    r->SetTarget();
    _area.w=w,_area.h=h;
}
Image::Image(Window* window,SDL_Rect& rect,SDL_Texture* t):Control(window){
    _area=rect;
    follow=false;
    _t=t;
}
Image::Image(Window* window,SDL_Point p,SDL_Texture* t):Control(window){
    _area.x=p.x,_area.y=p.y;
    SDL_QueryTexture(t,0,0,&_area.w,&_area.h);
    _t=t;
    follow=true;
}
void Image::_draw(){
    _belong->renderer->DrawTexture(NULL,&_area,_t);
}
void Image::SetImage(SDL_Texture* t){
    _t=t;
    if(follow){
        SDL_QueryTexture(t,0,0,&_area.w,&_area.h);
    }
}
void Image::SetImage(const char* path){
    SDL_DestroyTexture(_t);
    _t=_belong->renderer->LoadTexture(path);
    if(!_t){
        printf("Err:File %s not exist\n",path);
        throw FileNotFoundException();
    }
    if(follow){
        SDL_QueryTexture(_t,NULL,NULL,&_area.w,&_area.h);
    }
}
void Image::_press(int x,int y,int clicks,unsigned char key){}
void Image::_mouse_move(int x,int y){}
void Image::_release(int x,int y,int clicks,unsigned char key){}
Image::~Image(){if(_t)SDL_DestroyTexture(_t);}