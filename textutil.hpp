#pragma once
#include<SDL2/SDL_ttf.h>
#include<initializer_list>
#include<vector>

typedef struct TextMod{
    const char* text;
    SDL_Color color;
    unsigned int style;
    unsigned int hint;
    bool new_line;
    double scale;
}TextMod;

TextMod makeText(const char* text,
    SDL_Color color,
    unsigned int style=TTF_STYLE_NORMAL,
    unsigned int hint=TTF_HINTING_NORMAL,
    bool new_line=false,
    double scale=1.
);

SDL_Texture* BuildText(const std::initializer_list<TextMod>* mods,TTF_Font* font);
SDL_Texture* BuildText(const std::vector<TextMod>& mods,TTF_Font* font);
SDL_Texture* BuildText(std::initializer_list<TextMod> mods,TTF_Font* font);
SDL_Texture* BuildText(const std::initializer_list<TextMod>* mods,TTF_Font* font,
std::vector<std::pair<int,int>>& recv);
SDL_Texture* BuildText(std::initializer_list<TextMod> mods,TTF_Font* font,
std::vector<std::pair<int,int>>& recv);
SDL_Texture* BuildText(const std::vector<TextMod>& mods,TTF_Font* font,
std::vector<std::pair<int,int>>& recv);