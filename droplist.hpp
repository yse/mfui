#pragma once
#include "mfcore.hpp"

class DropList:public Control{
    protected:
    static void CheckMouse(void* l);
    Task* _task;
    size_t _select,_on;
    vector<string*>* _items;
    size_t cur_item;
    size_t max_item;
    int _h;
    SDL_Texture* _t,*_i;//选中显示|单项显示缓存
    //通过_items->operator[](_select)获取选中项
    virtual void OnSelect()=0;
    public:
    //未选中时返回-1
    size_t selected();
    bool extend;
    int item_height;
    SDL_Color background,line_color,disable_color,text_color,on_color,select_color;
    Font* font;
    //rect.w必须大于5 否则显示会出问题
    DropList(Window* window,SDL_Rect& rect,initializer_list<string*> init={}
    ,int item_height=15,size_t max_item=6,Font* font=default_font,
    SDL_Color text_color=Color::Black,SDL_Color background=Color::White,SDL_Color on_color=Color::SkyBlueGray,
    SDL_Color line_color=Color::Black,SDL_Color select_color=Color::Blue,SDL_Color disable_color=Color::Gray);
    DropList(Window* window);
    //w必须大于5 否则显示会出问题
    void SetSize(int w,int h)override;
    //最大展示数
    void SetMaxItem(size_t n);

    void Add(const string& str);
    //越界抛出out_of_range 返回该移除的string，需要手动析构
    string* RemoveItem(size_t index);
    //越界抛出out_of_range
    string* GetItem(size_t index);

    void _press(int x,int y,int clicks);

    void _draw();

    void _release(int x,int y,int clicks,unsigned char key)override;

    void _mouse_wheel(SDL_MouseWheelEvent& e)override;

    void _mouse_move(int x,int y)override;

    ~DropList()override;
    void LoseFocus();
};