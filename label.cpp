#include "label.hpp"

const char* Label::text(){return _text;}
SDL_Color Label::color(){return _color;}
Font* Label::font(){return _font;}

Label::Label(Window* window,SDL_Rect& rect,const char* text,SDL_Color color,Font* font):Control(window){
    _area=rect;
    follow=false;
    this->_text=text;
    this->_font=font;
    this->_color=color;
    _t=window->renderer->RenderText(font,text,color);
    SDL_QueryTexture(_t,NULL,NULL,&text_wh.x,&text_wh.y);
}

Label::Label(Window* window,SDL_Point pos,const char* text,SDL_Color color,Font* font):Control(window){
    _area.x=pos.x,_area.y=pos.y;
    follow=true;
    this->_text=text;
    this->_font=font;
    this->_color=color;
    _t=window->renderer->RenderText(font,text,color);
    SDL_QueryTexture(_t,NULL,NULL,&text_wh.x,&text_wh.y);
    _area.w=text_wh.x,_area.h=text_wh.y;
}
Label::Label(Window* window):Control(window){
    _text="";
    _font=default_font;
    follow=true;
    _color=Color::Black;
    _t=window->renderer->CreateTexture(0,0);
}
void Label::SetText(const char* new_text,Font* font,SDL_Color color){
    SDL_DestroyTexture(_t);
    _t=_belong->renderer->RenderText(font,new_text,color);
    _text=new_text,_color=color,_font=font;
    SDL_QueryTexture(_t,NULL,NULL,&text_wh.x,&text_wh.y);
    if(follow){
        _area.w=text_wh.x,_area.h=text_wh.y;
    }
}
void Label::_draw(){
    _belong->renderer->DrawTexture(NULL,&_area,_t);
}
void Label::_mouse_move(int x,int y){}
void Label::_press(int x,int y,int clicks,unsigned char key){}
void Label::_release(int x,int y,int clicks,unsigned char key){}
Label::~Label(){SDL_DestroyTexture(_t);}