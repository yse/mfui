#pragma once
#include "mfcore.hpp"

class WindowWithMenu:public Window{
    protected:
    MenuList* cur_list;
    vector<MenuList*>* extends;
    vector<MenuItem*>* items;//菜单栏
    bool menu_pressed,enable_top_menu;
    size_t select_item;
    SDL_Rect _r;
    bool _mouse_move(int x,int y)override;
    bool _mouse_release(int x,int y,int clicks,unsigned char key)override;
    void _draw()override;
    void OnWindowResize(int w,int h);
    public:
    SDL_Color on_color,normal_color,text_color;
    WindowWithMenu(const char* title,int w,int h,bool resize,bool full,bool enable_top_menu=true,
    SDL_Color on_color=Color::SkyBlueGray,SDL_Color normal_color=Color::Gray,SDL_Color text_color=Color::Black,
    SDL_Color back=Color::White,Window* parent=NULL,
    SDL_BlendMode mode=SDL_BLENDMODE_BLEND);
    ~WindowWithMenu()override;
    MenuItem* AddMenuItem(MenuItem* item);
    void ActiveMenu(MenuList* menu);
};