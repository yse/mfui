#include "textutil.hpp"
extern SDL_Renderer* gRender;
SDL_Texture* BuildText(std::initializer_list<TextMod> mods,TTF_Font* font){
    return BuildText(&mods,font);
}
SDL_Texture* BuildText(const std::initializer_list<TextMod>* mods,TTF_Font* font){
    auto style=TTF_GetFontStyle(font);
    auto hint=TTF_GetFontHinting(font);
    unsigned int nw=0,cw=0;
    unsigned int rh=0;
    unsigned int nh=0;
    for(auto& n:*mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        if(h>rh)rh=h;
        if(n.new_line){
            nh+=rh;
            if(cw>nw)nw=cw;
            cw=0;
            continue;
        }
        cw+=w;
    }
    nh+=rh;
    if(cw>nw)nw=cw;
    auto temp=SDL_CreateTexture(gRender,SDL_PIXELFORMAT_RGBA8888,
    SDL_TEXTUREACCESS_TARGET,nw,nh);
    SDL_SetTextureBlendMode(temp,SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(gRender,temp);
    SDL_SetRenderDrawColor(gRender,255,255,255,0);
    SDL_RenderClear(gRender);
    SDL_Rect r{0,0};
    rh=0;
    for(auto& n:*mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        r.w=w,r.h=h;
        auto s=TTF_RenderUTF8_Blended(font,n.text,n.color);
        auto t=SDL_CreateTextureFromSurface(gRender,s);
        SDL_FreeSurface(s);
        SDL_RenderCopy(gRender,t,0,&r);
        SDL_DestroyTexture(t);
        if(h>rh)rh=h;
        if(n.new_line){
            r.x=0,r.y+=rh;
            continue;
        }
        r.x+=r.w;
    }
    TTF_SetFontHinting(font,hint);
    TTF_SetFontStyle(font,style);
    SDL_SetRenderTarget(gRender,0);
    return temp;
}

SDL_Texture* BuildText(const std::vector<TextMod>& mods,TTF_Font* font){
    auto style=TTF_GetFontStyle(font);
    auto hint=TTF_GetFontHinting(font);
    unsigned int nw=0,cw=0;
    unsigned int rh=0;
    unsigned int nh=0;
    for(auto& n:mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        if(h>rh)rh=h;
        if(n.new_line){
            nh+=rh;
            if(cw>nw)nw=cw;
            cw=0;
            continue;
        }
        cw+=w;
    }
    nh+=rh;
    if(cw>nw)nw=cw;
    auto temp=SDL_CreateTexture(gRender,SDL_PIXELFORMAT_RGBA8888,
    SDL_TEXTUREACCESS_TARGET,nw,nh);
    SDL_SetTextureBlendMode(temp,SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(gRender,temp);
    SDL_SetRenderDrawColor(gRender,255,255,255,0);
    SDL_RenderClear(gRender);
    SDL_Rect r{0,0};
    rh=0;
    for(auto& n:mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        r.w=w,r.h=h;
        auto s=TTF_RenderUTF8_Blended(font,n.text,n.color);
        auto t=SDL_CreateTextureFromSurface(gRender,s);
        SDL_FreeSurface(s);
        SDL_RenderCopy(gRender,t,0,&r);
        SDL_DestroyTexture(t);
        if(h>rh)rh=h;
        if(n.new_line){
            r.x=0,r.y+=rh;
            continue;
        }
        r.x+=r.w;
    }
    TTF_SetFontHinting(font,hint);
    TTF_SetFontStyle(font,style);
    SDL_SetRenderTarget(gRender,0);
    return temp;
}

SDL_Texture* BuildText(std::initializer_list<TextMod> mods,TTF_Font* font,
std::vector<std::pair<int,int>>& recv){
    return BuildText(&mods,font,recv);
}

SDL_Texture* BuildText(const std::initializer_list<TextMod>* mods,TTF_Font* font,
std::vector<std::pair<int,int>>& recv){
    auto style=TTF_GetFontStyle(font);
    auto hint=TTF_GetFontHinting(font);
    unsigned int nw=0,cw=0;
    unsigned int rh=0;
    unsigned int nh=0;
    for(auto& n:*mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        if(h>rh)rh=h;
        cw+=w;
        if(n.new_line){
            recv.emplace_back(std::make_pair(cw,h));
            nh+=rh;
            if(cw>nw)nw=cw;
            cw=0;
            continue;
        }
    }
    nh+=rh;
    recv.emplace_back(std::make_pair(cw,rh));
    if(cw>nw)nw=cw;
    auto temp=SDL_CreateTexture(gRender,SDL_PIXELFORMAT_RGBA8888,
    SDL_TEXTUREACCESS_TARGET,nw,nh);
    SDL_SetTextureBlendMode(temp,SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(gRender,temp);
    SDL_SetRenderDrawColor(gRender,255,255,255,0);
    SDL_RenderClear(gRender);
    SDL_Rect r{0,0};
    rh=0;
    for(auto& n:*mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        r.w=w,r.h=h;
        auto s=TTF_RenderUTF8_Blended(font,n.text,n.color);
        auto t=SDL_CreateTextureFromSurface(gRender,s);
        SDL_FreeSurface(s);
        SDL_RenderCopy(gRender,t,0,&r);
        SDL_DestroyTexture(t);
        if(h>rh)rh=h;
        if(n.new_line){
            r.x=0,r.y+=rh;
            continue;
        }
        r.x+=r.w;
    }
    TTF_SetFontHinting(font,hint);
    TTF_SetFontStyle(font,style);
    SDL_SetRenderTarget(gRender,0);
    return temp;
}

TextMod makeText(const char* text,SDL_Color color,unsigned int style,unsigned int hint,bool new_line,double scale){
    return TextMod{text,color,style,hint,new_line,scale};
}

SDL_Texture* BuildText(const std::vector<TextMod>& mods,TTF_Font* font,
std::vector<std::pair<int,int>>& recv){
    auto style=TTF_GetFontStyle(font);
    auto hint=TTF_GetFontHinting(font);
    unsigned int nw=0,cw=0;
    unsigned int rh=0;
    unsigned int nh=0;
    for(auto& n:mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        if(h>rh)rh=h;
        cw+=w;
        if(n.new_line){
            recv.emplace_back(std::make_pair(cw,h));
            nh+=rh;
            if(cw>nw)nw=cw;
            cw=0;
            continue;
        }
    }
    nh+=rh;
    recv.emplace_back(std::make_pair(cw,rh));
    if(cw>nw)nw=cw;
    auto temp=SDL_CreateTexture(gRender,SDL_PIXELFORMAT_RGBA8888,
    SDL_TEXTUREACCESS_TARGET,nw,nh);
    SDL_SetTextureBlendMode(temp,SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(gRender,temp);
    SDL_SetRenderDrawColor(gRender,255,255,255,0);
    SDL_RenderClear(gRender);
    SDL_Rect r{0,0};
    rh=0;
    for(auto& n:mods){
        int w,h;
        TTF_SetFontStyle(font,n.style);
        TTF_SetFontHinting(font,n.hint);
        TTF_SizeUTF8(font,n.text,&w,&h);
        w*=n.scale,h*=n.scale;
        r.w=w,r.h=h;
        auto s=TTF_RenderUTF8_Blended(font,n.text,n.color);
        auto t=SDL_CreateTextureFromSurface(gRender,s);
        SDL_FreeSurface(s);
        SDL_RenderCopy(gRender,t,0,&r);
        SDL_DestroyTexture(t);
        if(h>rh)rh=h;
        if(n.new_line){
            r.x=0,r.y+=rh;
            continue;
        }
        r.x+=r.w;
    }
    TTF_SetFontHinting(font,hint);
    TTF_SetFontStyle(font,style);
    SDL_SetRenderTarget(gRender,0);
    return temp;
}