#pragma once
#include "mfcore.hpp"

class Canvas:public Control{
    protected:
    SDL_Texture* _t;
    public:
    SDL_Color background;
    //所有绘制API都基于画布坐标系
    Canvas(Window* window,SDL_Rect& rect,SDL_Color background=Color::White);
    //加入window前请务必调用SetSize重置画布大小
    Canvas(Window* window);
    void DrawLine(SDL_Point p1,SDL_Point p2,SDL_Color color);
    void DrawRect(SDL_Rect& rect,SDL_Color color);
    void FillRect(SDL_Rect& rect,SDL_Color color);
    void DrawPoint(SDL_Point p,SDL_Color color);
    void DrawTexture(SDL_Texture* texture,SDL_Rect& rect);
    void DrawText(SDL_Point p,Font* font,char* text,SDL_Color color);
    void SetSize(int w,int h);
    void Clear();
    ~Canvas()override;
};