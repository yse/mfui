#pragma once
#include "mfcore.hpp"

class Label:public Control{
    protected:
    const char* _text;
    SDL_Texture* _t;
    SDL_Point text_wh;
    Font* _font;
    SDL_Color _color;
    public:
    const char* text();
    SDL_Color color();
    Font* font();
    bool follow;//绘制大小是否随文字渲染变化而变化
    //使用指定大小创建
    Label(Window* window,SDL_Rect& rect,const char* text="label",SDL_Color color=Color::Black,Font* font=default_font);
    //使用文字渲染后的大小创建
    Label(Window* window,SDL_Point pos,const char* text="label",SDL_Color color=Color::Black,Font* font=default_font);
    Label(Window* window);
    void SetText(const char* new_text,Font* font=default_font,SDL_Color color=Color::Black);
    void _draw();
    void _mouse_move(int x,int y);
    void _press(int x,int y,int clicks,unsigned char key);
    void _release(int x,int y,int clicks,unsigned char key);
    ~Label()override;
};