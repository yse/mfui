#pragma once
#include "mfcore.hpp"

class CheckBox:public Control{
    protected:
    virtual void OnChanged()=0;
    public:
    bool value;
    SDL_Color background,line_color,disable_color;
    CheckBox(Window* window,SDL_Rect& rect,bool init=false,SDL_Color back=Color::White,SDL_Color line=Color::Black,
    SDL_Color disable=Color::Gray);
    CheckBox(Window* window);
    void _press(int x,int y,int clicks,unsigned char key)override;
    void _release(int x,int y,int clicks,unsigned char key)override;
    void _mouse_move(int x,int y);
    void _draw();
    ~CheckBox()override;
};