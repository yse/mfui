#pragma once
#include "mfcore.hpp"
class Image:public Control{
    protected:
    SDL_Texture* _t;
    public:
    bool follow;//是否使用图片变化后加载的图片大小
    //使用指定绘制大小 找不到path时抛出FileNotFoundException
    Image(Window* window,SDL_Rect& rect,const char* path);
    //使用图像大小 找不到path时抛出FileNotFoundException
    Image(Window* window,SDL_Point pos,const char* path);
    //使用给定纹理 会在析构时释放
    Image(Window* window,SDL_Rect& rect,SDL_Texture* t);
    //使用给定纹理 会在析构时释放
    Image(Window* window,SDL_Point p,SDL_Texture* t);
    //添加到window前请务必调用SetImage设置好图片
    Image(Window* window,unsigned int w,unsigned int h);
    void _draw();
    void SetImage(const char* path);
    //使用给定纹理 会在析构时释放
    void SetImage(SDL_Texture* t);
    void _press(int x,int y,int clicks,unsigned char key);
    void _mouse_move(int x,int y);
    void _release(int x,int y,int clicks,unsigned char key);
    ~Image()override;
};