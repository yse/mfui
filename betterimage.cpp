#include "betterimage.hpp"

BetterImage::BetterImage(Window* w,SDL_Point p,char* path,double angle,
SDL_RendererFlip flip):Image(w,p,path),angle(angle),flip(flip){}

BetterImage::~BetterImage(){}

void BetterImage::_draw(){
    _belong->renderer->DrawTexture(0,&_area,_t,angle,flip);
}