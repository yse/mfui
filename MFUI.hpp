#pragma once
#include<SDL2/SDL_image.h>

#include<list>
#include<string>

#include "label.hpp"
#include "droplist.hpp"
#include "betterimage.hpp"
#include "mfmenu.hpp"
#include "inputbox.hpp"
#include "listview.hpp"
#include "button.hpp"
#include "textbutton.hpp"
#include "canvas.hpp"
#include "checkbox.hpp"
#include "progressbar.hpp"

void CloseWindow(unsigned int id);//强制关闭指定窗口
list<string*>* GetDirAllFile(const char* path);
int MFMain(Uint32 *delta_receiver);
void MFQuit();
void MFInit(int font_size);