#include "progressbar.hpp"

bool ProgressBar::SetProgress(double v){
    if(0.<=v&&v<=1.){
        value=v;
        OnChanged(v);
        return true;
    }
    return false;
}

double ProgressBar::GetProgress(){return value;}

void ProgressBar::_mouse_move(int x,int y){
    if(SDL_GetMouseState(0,0)&SDL_BUTTON(SDL_BUTTON_LEFT)){
        value=(x-_area.x)/(double)_area.w;
        OnChanged(value);
    }
}
void ProgressBar::_press(int x,int y,int clicks,unsigned char key){
    value=(x-_area.x)/(double)_area.w;
    OnChanged(value);
}
void ProgressBar::_draw(){
    auto r=_belong->renderer;
    r->SetColor(back);
    r->FillRect(_area);
    SDL_Rect rect{_area.x+bound,_area.y+bound,(int)((_area.w-bound-bound)*value)
    ,_area.h-bound-bound};
    if(_enabled){
        r->SetColor(front);
        r->FillRect(rect);    
        return;
    }
    SDL_Color c{front.r,front.g,front.b,(unsigned char)(front.a>>1)};
    r->SetColor(c);
    r->FillRect(rect);
}

ProgressBar::ProgressBar(Window* w,SDL_Rect& r,double init,SDL_Color back,
SDL_Color front,int bound):Control(w),back(back),front(front),bound(bound){
    _area=r;
    if(0.<=init&&init<=1.){
        value=init;
        return;
    }
    value=1.;
}
ProgressBar::ProgressBar(Window* w):Control(w){
    _area={0,0,0,0};
    value=1.;
    front=Color::ManganeseBlue;
    back=Color::Black;
    bound=3;
}
ProgressBar::~ProgressBar(){}